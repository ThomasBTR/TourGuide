<img src="https://img.shields.io/badge/java-%23ED8B00.svg?&style=for-the-badge&logo=java&logoColor=white"/> * * *  <img src="https://img.shields.io/badge/spring%20-%236DB33F.svg?&style=for-the-badge&logo=spring&logoColor=white"/> 
 * * * <img src="https://img.shields.io/badge/Gradle-computable-blue"> * * * <img src="https://img.shields.io/badge/SonarCloud-Coverage-lightgrey"> 

# Tour Guide

An application to know the nearest attractions and to earn discounts on hotels and attractions.
This app uses Java 8 to run.

### Architecture diagram

![Alt text](docs/Architectural diagram.jpg "Architecture diagram of Tourguide")

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes.

### Prerequisites

What things you need to install the software and how to install them

- Java 1.8
- Maven 4.0.0

### Endpoints available

* GET / : index
* GET /location(userName) : retrieve VisitedLocation location for a specific user with <userName> query param
* GET /nearbyAttractions(userName) : retrieve a NearbyAttraction list for a specific user with <userName> query param
* GET /rewards(userName) : retrieve a UserReward list for a specific user with <userName> query param
* GET /tripDeals(userName) : retrieve a Provider list for a specific user with <userName> query param
* POST /userPreferences(userName, numberOfChildren, numberOfAdult, tripDuration) : update a UserPreferences for a
  specific user from a query params userName, numberOfChildren, numberOfAdult, tripDuration

### Installing

A step by step series of examples that tell you how to get a development env running:

1.Install Java:

[Java installation link](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)

2.Install Gradle:

[Gradle installation link](https://gradle.org/releases/)

### Running App

You can run the application in two different ways:

1/ import the code into an IDE of your choice and run the Application.java to launch the application.

2/ Or import the code, unzip it, open the console, go to the root folder that contains the gradlew file, then execute
the below command to launch the application.

```bash
gradlew bootRun
```

### Testing

The app has unit tests written.

To run the tests from maven, go to the folder that contains the pom.xml file and execute the below command.

```bash
gradlew build
```

### Technical and Functional Specifications

The specifications were showed in the demo. Please contact the current team in charge of this application to access
these infos.

### Coverage

The tests coverage can be accessed after building the app.
Access to the current coverage results are accessible on SonarCloud here:
[TourGuide SonarCloud](https://sonarcloud.io/project/overview?id=ThomasBTR_TourGuide)
