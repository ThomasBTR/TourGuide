# Tour Guide

## 2022-10-03 - 2.0.0

### Global upgrade

- Gitlab CI set up
- SonarCloud checks set up

### Technical upgrade

- Global refacto endpoint, remove HTTP verbose in path and specify RequestMapping type
- refacto package and classes location to improve code reading and accessibility
- Improved calculateReward with Threadpool
- Improved trackUserLocation with Threadpool

____

- new Endpoint POST /userPreference
- implemented Endpoint GET /nearbyAttractions(userName)
- implemented Endpoint GET /allCurrentLocations

____

- fixed @Ignore tests
- Implemented classes tested