package tourguide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourguide.helper.InternalTestHelper;
import tourguide.models.User;
import tourguide.services.RewardsService;
import tourguide.services.TourGuideService;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
public class PerformanceIT {

    /*
     * A note on performance improvements:
     *
     *     The number of users generated for the high volume tests can be easily adjusted via this method:
     *
     *     		InternalTestHelper.setInternalUserNumber(100000);
     *
     *
     *     These tests can be modified to suit new solutions, just as long as the performance metrics
     *     at the end of the tests remains consistent.
     *
     *     These are performance metrics that we are trying to hit:
     *
     *     highVolumeTrackLocation: 100,000 users within 15 minutes:
     *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
     *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     */

    @Test
    public void highVolumeTrackLocation() {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        InternalTestHelper.setInternalUserNumber(100000);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        List<User> allUsers = tourGuideService.getAllUsers();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        CopyOnWriteArrayList<CompletableFuture<VisitedLocation>> trackAllUsers = new CopyOnWriteArrayList<>();

        for (User user : allUsers) {
            trackAllUsers.add(tourGuideService.trackUserLocation(user));
        }
        await().atMost(15, TimeUnit.MINUTES).until(() -> trackAllUsers.stream().allMatch(CompletableFuture::isDone));
        stopWatch.stop();
        tourGuideService.tracker.stopTracking();
        tourGuideService.shutdownExecutorService();

        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertThat(TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())).isLessThan(TimeUnit.MINUTES.toSeconds(15));
    }

    @Test
    public void highVolumeGetRewards() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());

        // Users should be incremented up to 100,000, and test finishes within 20 minutes
        InternalTestHelper.setInternalUserNumber(100000);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        Attraction attraction = gpsUtil.getAttractions().get(0);
        List<User> allUsers;
        allUsers = tourGuideService.getAllUsers();
        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        for (User user : allUsers) {
            assertThat(user.getVisitedLocations()).isNotEmpty();
        }

        allUsers.forEach(rewardsService::calculateRewards);

        await().atMost(20, TimeUnit.MINUTES)
                .until(() -> allUsers.stream().noneMatch(user -> user.getUserRewards().isEmpty()));

        rewardsService.shutdownExecutor();

        System.out.println("User rewards calculated for each user");

        for (User user : allUsers) {
            assertThat(user.getUserRewards()).isNotEmpty();
        }
        stopWatch.stop();
        tourGuideService.tracker.stopTracking();

        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertThat(TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())).isLessThan(TimeUnit.MINUTES.toSeconds(20));
    }

}
